[![SensioLabsInsight](https://insight.sensiolabs.com/projects/2dfee035-5c07-4404-9132-05e3120f0a66/mini.png)](https://insight.sensiolabs.com/projects/2dfee035-5c07-4404-9132-05e3120f0a66)
[![Build Status](https://travis-ci.org/djsmithme/Database.svg?branch=master)](https://travis-ci.org/djsmithme/Database)
[![Test Coverage](https://codeclimate.com/github/djsmithme/Database/badges/coverage.svg)](https://codeclimate.com/github/djsmithme/Database/coverage)
[![Codacy Badge](https://api.codacy.com/project/badge/grade/51e368d921d5493eae90a6a12fd35596)](https://www.codacy.com/app/dansmith/Database)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/djsmithme/Database/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/djsmithme/Database/?branch=master)


# Database
Database wrapper with PDO adaptor.

```
$database = new \Ds\Database\Database(
    new \Ds\Database\Connections\PDOConnection('localhost','database','user','password'),
    new \Ds\Cache\Cache(),
    new \Ds\Authenticate\Token()
);

//Query Database
$data = $database->fetch('SELECT `column` FROM table',[]);

//Loop through results
foreach ($data as $result){
    echo $result->column;
}

//Cache Results using memcache
$cache =new \Ds\Cache\Cache(
    new \Ds\Cache\MemcacheStorage(
        new \Memcached()
    )
);

$database = $database->withCacheInterface($cache);
$data = $database->fetch('SELECT `column_a` FROM table',[], 360); //lookup database;
$data = $database->fetch('SELECT `column_a` FROM table',[], 360); //if requested within 360 seconds fetched from memcache;

