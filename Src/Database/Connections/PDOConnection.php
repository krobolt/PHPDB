<?php

namespace Ds\Database\Connections;

use Ds\Database\DatabaseException;
use Ds\Database\ConnectionInterface;
use \PDO;
use \PDOException;

/**
 * Class PDOConnection
 *
 * @package Ds\Database\Connections
 * @author  Dan Smith <dan--smith@hotmail.co.uk>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */
class PDOConnection implements ConnectionInterface
{

    protected $connection;

    protected $lastId = null;

    protected $options;

    public function __construct($dsn = null, $username = '', $password = '', array $options = [])
    {
        $this->setOptions($options);
        if ($dsn) {
            try {
                $this->connection = new PDO(
                    $dsn,
                    $username,
                    $password,
                    $this->options
                );
            } catch (\PDOException $e) {
                throw new DatabaseException($e->getMessage());
            }
        }
    }

    /**
     * Define Options for connection.
     *
     * @param  array $options
     * @return array
     */
    public function setOptions(array $options = [])
    {
        if (empty($options)) {
            // generate a database connection, using the PDO connector
            $options = [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
            ];
        }

        return $this->options = $options;
    }

    /**
     * Replace Connection
     *
     * @param  PDO $pdo
     * @return PDOConnection
     */
    public function withConnection(\PDO $pdo)
    {
        $new = clone $this;
        $new->connection = $pdo;
        return $new;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function getLastId()
    {
        return $this->lastId;
    }


    /**
     * @param string $statement
     * @param array  $args
     * @param array  $options
     * @return bool
     * @throws DatabaseException
     */
    public function fetch($statement, array $args = [], array $options = [])
    {
        try {
            $results = false;
            $pdo = $this->connection->prepare($statement);
            if ($pdo) {
                $pdo->execute($args);
                $results = $pdo->fetchAll();
            }
            return $results;
        } catch (PDOException $e) {
            throw new DatabaseException($e->getMessage());
        }
    }

    /**
     * @param string $statement
     * @param array  $args
     * @param array  $options
     * @return bool
     * @throws DatabaseException
     */
    public function update($statement, array $args = [], array $options = [])
    {
        try {
            $pdo = $this->connection->prepare($statement);
            if ($pdo) {
                $pdo->execute($args);
                $this->lastId = $this->connection->lastInsertId();
                return true;
            }
            return false;
        } catch (PDOException $e) {
            throw new DatabaseException($e->getMessage());
        }
    }

    /**
     * Begin Transaction.
     */
    public function beginTransaction()
    {
        $this->connection->beginTransaction();
    }

    /**
     * Rollback Transaction.
     */
    public function rollBack()
    {
        $this->connection->rollBack();
    }

    /**
     * Commit Transaction.
     */
    public function commit()
    {
        $this->connection->commit();
    }
}
