<?php

namespace Ds\Database;

/**
 * Interface ConnectionInterface
 *
 * Database Connection Interface.
 *
 * @package Ds\Database
 * @author  Dan Smith <dan--smith@hotmail.co.uk>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */
interface ConnectionInterface
{
    /**
     * Executes an SQL statement in a single function call, returning the response returned by the statement.
     *
     * @param  string $statement SQL Prepared statement.
     * @param  array  $args      Statement variable parameters.
     * @param  array  $options   Additional options to be passed to query.
     * @return bool
     * @throws DatabaseException
     */
    public function update($statement, array $args = [], array $options = []);

    /**
     * Executes an SQL statement in a single function call, returning the data returned by the statement.
     *
     * @param  string $statement SQL Prepared statement.
     * @param  array  $args      Statement variable parameters.
     * @param  array  $options   Additional options to be passed to query.
     * @return mixed
     * @throws \Exception
     */
    public function fetch($statement, array $args = [], array $options = []);
}
